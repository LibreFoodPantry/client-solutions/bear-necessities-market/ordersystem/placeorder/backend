FROM alpine:3.9

ENV METEOR_VERSION=1.8.1
ENV METEOR_ALLOW_SUPERUSER true
ENV NODE_VERSION 8.15
RUN apk add --no-cache --repository=http://dl-cdn.alpinelinux.org/alpine/v3.8/main/ nodejs=8.14.0-r0 npm 

RUN node --version

RUN mkdir -p /srv/app/Backend
WORKDIR /srv/app/Backend

COPY package.json /srv/app/Backend
COPY package-lock.json /srv/app/Backend

RUN npm install --silent

COPY . /srv/app/Backend

EXPOSE 4000

CMD ["mongod"]
CMD ["node", "server.js"]
