<!--
Please complete this template in its entirety for your sprint retrospective.
Name your file as shown:  Name - Sprint # Retrospective
-->

## Links to evidence of activity on GitLab with one sentence description for each link.

<!--
Example:
#1 - Description for issue 1. \
#2 - Description for issue 2. \
#3 - Description for issue 3.
-->

## Reflection on what worked well

## Reflection on what didn't work well

## Reflection on what changes could be made to improve as a team

## Reflection on what changes could be made to improve as an individual

## Reflection on how you are performing in your role this sprint
