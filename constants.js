/*
THIS FILE SHOULD BE ADDED TO GITIGNORE WHEN PRODUCTION READY SINCE IT WOULD CONTAIN
SECRET ACCESSERS AND DATA!

This file is intended to hold all constant variables across the backend project.  

Variables are accessible by using: const dotenv = require('dotenv');

Format: const NAME = process.env.NAME;
   Optional: || hardcoded_value;
*/

// Server
// Use port 4000 unless preconfigured.
module.exports.PORT = process.env.PORT || 4000;
// Use port 3000 unless preconfigured
module.exports.HOMEPORT = process.env.HOMEPORT || 3000;
// Use mongo unless preconfigured
module.exports.MONGOPORT = process.env.MONGOPORT || "mongo";


// Debug
// Check type to work around browser 'reference error' then check if 
// callback is from this file
if (typeof require !== 'undefined' && require.main === module) {
   console.log("PORT:", module.exports.PORT);
   console.log("HOMEPORT:", module.exports.HOMEPORT);
   console.log("MONGOPORT:", module.exports.MONGOPORT);
}