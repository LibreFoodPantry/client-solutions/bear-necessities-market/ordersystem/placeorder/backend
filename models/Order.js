const mongoose = require('mongoose');

const OrderSchema = mongoose.Schema({
    orderType:{
        type: String
    },
    itemsOrdered: {
        type: String
    },
    dietaryRestrictions: {
        type: String
    },
    commentsPreferences: {
        type: String
    },
    email: {
        type: String,
        lowercase: true
    },
    emailVerified: {
        type: Boolean
    },
    dateOrdered: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Orders', OrderSchema);
