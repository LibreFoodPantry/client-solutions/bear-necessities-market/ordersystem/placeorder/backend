<h1>API Developer Documentation</h1>

<br>**File:** [Orders.js](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/ordersystem/placeorder/placeordermodule/-/blob/master/Backend/routes/orders.js)
<br>**Language:** [JavaScript](https://www.javascript.com/)
<br>**Note:** Before using the API, make sure the Homepage `http://localhost:4000/api` returns "**Place Order API**" with status code **200**.
<br>**Version:** 1.0

<h3>GET all the orders</h3>

---
**GET** - `http://localhost:4000/api/orders/get`
<br>**Status**
- `200` - **OK** - All the orders have been returned
- `404` - **Not Found** - There are no orders

<br>**Return Format**: XML

<h3>GET a order with the given order ID</h3>

---
**GET** - `http://locahost:4000/api/orders/get/:orderID`
<br>**Status**
- `200` - **OK** - The specific order has been returned
- `404` - **Not Found** - No order exists with given order ID

<br>**Return Format**: XML

<h3>GET all the orders placed by the given email ID</h3>

---
**GET** - `http://locahost:4000/api/orders/get/email/:emailID`
<br>**Status**
- `200` - **OK** - All the orders placed by given email ID have been returned
- `404` - **Not Found** - The email ID hasn't placed any orders

<br>**Return Format**: XML

<h3>GET all the orders placed on the given date and time</h3>

---
**GET** - `http://locahost:4000/api/orders/get/date/:orderDate`
<br>**Status**
- `200` - **OK** - All the orders placed on given timestamp have been returned
- `404` - **Not Found** - There are no orders placed on that timestamp

<br>**Return Format**: XML

<h3>POST a new order</h3>

---
**POST** - `http://locahost:4000/api/orders/post`
<br>**Status**
- `201` - **Created** - The orders has been created
- `204` - **No Content** - The request has incorrect format

<br>**Arguments**
- orderType
  - A **String** value to show what type of order is place:
    - Golden Bear Order
    - Custom Order
- itemsOrdered
  - If orderType is "**Golden Bear Order**" then this field is an **empty string**. However, if the orderType is "Custom Order" then the **items ordered are listed and seperated by , (comma)**.
- dietaryRestrictions
  - If the order has any dietary restriction or allergies then they are listed here as a **paragraph**.
- commentsPreferences
  - If the order has any specific comments or preferences regarding any item in the order then they are listed here as a **paragraph**.
- email
  - The **String** value of person's email who is placing the order.
- emailVerified
  - A **boolean** value that is a confirmation of the user's email ID match in two different fields. If the system is not checking for it, this can be passed as **true**.
- dateOrdered **(optional)**
  - The **timestamp** the order was placed. If not provided the current system timestamp will be considered.

<br>**Arguments Format**: JSON

<br>**Return Format**: XML

<br>**Example Request**
```
$.ajax({
            url: 'http://localhost:4000/api/orders/post',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                orderType: 'Custom Order',
                itemsOrdered: 'Pasta, Tuna, Canned Fruits',
                dietaryRestrictions: 'No restrictions',
                commentsPreferences: 'Pineapple fruit',
                email: 'fName.lName@wne.edu',
                emailVerified: true
            }),
            dataType: 'json'
        });
```

<br>**Example Response**
```
{
    "_id": "6075f6712c0348e20f4ad2bb",
    "orderType": "Custom Order",
    "itemsOrdered": "Pasta, Tuna, Canned Fruits",
    "dietaryRestrictions": "No restrictions",
    "commentsPreferences": "Pineapple fruit",
    "email": "fName.lName@wne.edu",
    "emailVerified": true,
    "dateOrdered": "2021-04-13T19:52:17.506Z",
    "__v": 0
}
```

<h3>DELETE a order with the given order ID</h3>

---
**DELETE** - `https://localhost:4000/api/orders/delete/:orderID`
<br>**Status**
- `200` - **OK** - The order with the given order ID has been deleted
- `404` - **Not Found** - No order exists with given order ID

<br>**Return Format**: XML